package com.thisismartin.example.controller;

import com.thisismartin.example.dto.CommentDTO;
import com.thisismartin.example.service.CommentsService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: Martin.Spasovski
 * Date: 5/31/13
 * Time: 11:49 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class MainController {

    @Resource
    CommentsService commentsService;

    /**
     * Returns the main page when the webapp is accessed.
     *
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "main";
    }

    @RequestMapping(value = "/comments", method = RequestMethod.GET, produces = "application/json")
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public List<CommentDTO> get()  {

        return commentsService.getAll();
    }

    @RequestMapping(value = "/comments", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public List<CommentDTO> post(@RequestBody final CommentDTO updated) {

        commentsService.add(updated);
        return commentsService.getAll();
    }

}
